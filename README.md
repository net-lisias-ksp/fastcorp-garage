# FASTCORP Autogarage :: UNNOFFICIAL

This is an unofficial, non authorised repository for **FASTCORP Autogarage** for historical reference and troubleshooting.


## Content
* [Binaries](https://github.com/net-lisias-ksph/FASTCORP-Autogarage/tree/Archive)
* Sources
	+ WiP
* [Change Log](./CHANGE_LOG.md)


## License

Licensed under [CC-BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)

You are authorised to fork this repository under GitHub [ToS](https://help.github.com/articles/github-terms-of-service/).


## References

* [fast_de_la_speed](https://forum.kerbalspaceprogram.com/index.php?/profile/111433-fast_de_la_speed/) ROOT
    + [Forum](https://forum.kerbalspaceprogram.com/index.php?/topic/127408-*)
    + [SpaceDock](https://spacedock.info/mod/771/FASTCORP%20Autogarage)
	+ IMGUR
		- [Release 1](https://imgur.com/a/ep3nA)
		- [Release 2](https://imgur.com/a/g3GP0)
		- [Release 3](https://imgur.com/a/SM5oH)
